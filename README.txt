
Google Analytics Track YouTube


Description
===========

Track YouTube Player Events in Google Analytics.

Requirements
============

* Google Analytics <https://www.drupal.org/project/google_analytics>
* Google Analytics account <https://www.google.com/analytics>
* lunametrics-youtube.gtm.js script 
  <https://github.com/lunametrics/youtube-google-analytics>

Installation
============

* Download the lunametrics-youtube.gtm.js script from 
  https://github.com/lunametrics/youtube-google-analytics 
  and install to /libraries/youtube-google-analytics/
* Enable module

Usage
=====

By default, the lunametrics-youtube.gtm.js script will be attached to all pages. 
To modify which pages the youtube-google-analytics script is attached to use 
hook_page_attachments() override with your custom logic.

The script will send tracking events when users Play, Pause, Watch to End, and 
watch 10%, 25%, 50%, 75%, and 90% of each video on the page. These defaults can 
be adjusted by modifying the object passed as the third argument at the bottom 
of the lunametrics-youtube.gtm.js script.

Acknowledgements
============

Matt West <https://www.drupal.org/u/mattdanger> @ SochaDev 
<https://www.drupal.org/node/2439991> 

This module is nothing without the wonderful youtube-google-analytics script 
from LunaMetrics <http://www.lunametrics.com/>
